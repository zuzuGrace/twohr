# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/


$(document).on "turbolinks:load", ->
		$('.home-header').click ->
			$('.explain').fadeOut(250)
			$('.exp-def').fadeOut(250)
			$('.access-forms').fadeOut(250)
			$('.footer-content').fadeOut(250)
			$('#home-opts-content').fadeIn(250)
			if @id == 'intro'
				$('#intro-explain').fadeIn(550)
			else if @id == 'hiw'
				$('#hiw-explain').fadeIn(550)
			else if @id == 'food'
				$('#food-explain').fadeIn(550)
			else if @id == 'more'
				$('#more-explain').fadeIn(550)
			else if @id == 'ideal'
				$('#ideal-explain').fadeIn(550)

		$('.home-header').mouseenter ->
			$(this).css('letter-spacing', '4px')

		$('.home-header').mouseleave ->
			$(this).css('letter-spacing', '2px')

		$('.footer-bttn').click ->
			$('.explain').fadeOut()
			$('.home-header').fadeOut()
			$('.footer-content').fadeOut(250)
			$('#home-opts-content').fadeOut(250)
			if @id == 'hp'
				$('#health-policy').fadeIn(250)
			else if @id == 'terms'
				$('#termsc').fadeIn(250)


		$('.bttn').click ->
			$('#home-opts-content').fadeOut(250)
			$('.footer-content').fadeOut(250)
			$('.access-forms').fadeOut(250)
			$('.home-header').fadeIn(250)
			if @id == 'signup-bttn'
				$('#signup-form').fadeIn(550)
			else if @id == 'login-bttn'
				$('#login-form').fadeIn(550)

