# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/


$(document).on "turbolinks:load", ->	
		$('#dailyCheckIn').click ->
				document.getElementById("dailyCheckIn").innerHTML = "CheckedIn"
				$(this).css("background-color", "#8AC054")
				$('#checkIn_dailyCheckIn').val(true)
		$('.acct-bttn').click -> 
			$('#buyer-dashboard').css('opacity', '0.25')
			$('#edit-acct-form').fadeIn(550)

		$('#update-acct').click ->
			$('#buyer-dashboard').css('opacity', '1')
			$('.acct-forms').fadeOut(250)
