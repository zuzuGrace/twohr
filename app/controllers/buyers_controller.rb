class BuyersController < ApplicationController
	def index
		@buyers = Buyers.all
	end

	def show
		@buyer = Buyer.find(params[:id])
		@nutrition = Nutrition101.all
	end

	def create 
		@buyer = Buyer.new(buyer_params)
		@buyer.first_signup = true
		@buyer.new_session = true
		@buyer.session_num = 0
		if @buyer.save
			#handle saving registreation data
			log_in @buyer
			flash[:success] = "Welcome to your Dashboard"
			redirect_to @buyer
		else 
			redirect_to root_url
		end
	end


	def update
		@buyer = Buyer.find(params[:id])
		if @buyer.update_attributes(buyer_params)
			flash[:success] = "Updated"
		else 
			flash[:error] = "Unable to update"
		end
	end

	def edit
		@buyer = Buyer.find_by(params[:id])
	end

	def destroy
		@buyer  = Buyer.find(params[:id])
		@buyer.destroy
	end 

	private
		def buyer_params
			params.require(:buyer).permit(:username, :email, :password, :password_confirmation)
		end

end
