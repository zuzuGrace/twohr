class DailyTrackersController < ApplicationController
	def create
		@daily = current_user.daily_trackers.build(daily_tracker_params)
		@daily.buyer_id = current_user.id
		@daily.dayNumber = 0
		@daily.dailyReportedMealPercentage = 0.0
		if @daily.save
			flash[:success] = 'Welcome to today\'s practice.'
		else 
			flash[:error] = 'Cannot checkIn. Please try again.'
		end
	end

	private
		def daily_tracker_params
			params.require(:checkIn).permit(:dailyCheckIn)
		end

end
