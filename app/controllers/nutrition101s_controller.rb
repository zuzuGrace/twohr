class Nutrition101sController < ApplicationController
	def create
		@nutri = Nutrition101s.new(nutri_params)
		if @nutri.save
			flash[:success] = 'Created Nutrition Suggestion'
		else
			flash[:error] = 'Unable to create Nutrition Suggestion'
		end
	end

	def update
		@nutri = Nutrition101.find(params[:id])
		if @nutri.update_attributes(nutri_params)
			flash[:success] = "Updated Nutrition Suggestion"
		else 
			flash[:error] = "Unable to update yNutrition Suggestion"
		end
	end

	def edit
		@nutri = Nutrition101.find_by(params[:id])
	end

	def destroy
		@nutri  = Nutrition101.find(params[:id])
		@nutri .destroy
	end 

	private 
		def nutri_params
			params.require(:suggestion).permit(:suggestion)
		end
end
