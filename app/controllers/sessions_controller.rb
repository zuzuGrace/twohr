class SessionsController < ApplicationController
	def create
			buyer = Buyer.find_by(email: params[:session][:email].downcase)
			if buyer && buyer.authenticate(params[:session][:password])
				log_in buyer
				redirect_to buyer
			else
				flash.now[:error] = "Invalid LogIn"
				redirect_to root_url
			end
	end

	def destroy
		log_out
		redirect_to root_url
	end
end
