module SessionsHelper
	def log_in(buyer)
		session[:buyer_id] = buyer.id
	end

	#returns currently logged in user
	def current_user
		if session[:buyer_id]
		 	 @current_user ||= Buyer.find_by(id: session[:buyer_id])
		end
	end

	def logged_in?
		!current_user.nil?
	end

	#logs out current user
	def log_out
		session.delete(:buyer_id)
		@current_user = nil
	end


	#login the overseers
	def overseer_log_in(overseer)
		session[:overseer_id] = overseer.id
	end


	def current_overseer
		if session[:overseer_id]
			@current_overseer ||= Overseer.find_by(id: session[overseer_id])
		end
	end

	def overseer_logged_in?
		!current_overseer.nil?
	end

	def overseer_logged_out
		session.delete(:overseer_id)
		@current_overseer
	end

	
end
