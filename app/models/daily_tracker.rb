class DailyTracker < ApplicationRecord
  belongs_to :buyer
  validates :buyer_id, presence: true
  validates :dayNumber, presence: true
  validates :dailyCheckIn, presence: true
  validates :dailyReportedMealPercentage, presence: true
end
