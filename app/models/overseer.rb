class Overseer < ApplicationRecord
	before_save{username.downcase!}
	has_secure_password
	 validates :password, presence: true, length: { minimum: 6 }
end
