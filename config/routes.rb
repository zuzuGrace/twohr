Rails.application.routes.draw do
  root 'app_pages#home'
  post '/start', to: 'buyers#create'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  post '/login', to: 'sessions#create'
  delete '/logout', to: 'sessions#destroy'
  post '/create_daily_tracker', to: 'daily_trackers#create'

  resources :buyers do
  		resources :daily_trackers, only: [:index, :create]
  		resources :buyer_session, only: [:index, :create]
 	end

  resources :nutrition101s
end
