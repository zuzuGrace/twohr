class CreateOverseers < ActiveRecord::Migration[5.2]
  def change
    create_table :overseers do |t|
      t.string :username

      t.timestamps
    end
  end
end
