class AddPasswordDigestToOverseers < ActiveRecord::Migration[5.2]
  def change
    add_column :overseers, :password_digest, :string
  end
end
