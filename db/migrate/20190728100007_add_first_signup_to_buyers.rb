class AddFirstSignupToBuyers < ActiveRecord::Migration[5.2]
  def change
    add_column :buyers, :first_signup, :boolean
  end
end
