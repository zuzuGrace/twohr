class CreateDailyTrackers < ActiveRecord::Migration[5.2]
  def change
    create_table :daily_trackers do |t|
      t.integer :dayNumber
      t.boolean :dailyCheckIn
      t.string :dailyReportedMealPercentage
      t.references :buyer, foreign_key: true

      t.timestamps
    end
  end
end
