class CreateNutrition101s < ActiveRecord::Migration[5.2]
  def change
    create_table :nutrition101s do |t|
      t.text :suggestion

      t.timestamps
    end
  end
end
