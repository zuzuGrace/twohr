class AddNewSessionToBuyers < ActiveRecord::Migration[5.2]
  def change
    add_column :buyers, :new_session, :boolean
  end
end
