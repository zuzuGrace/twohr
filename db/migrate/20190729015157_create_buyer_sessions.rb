class CreateBuyerSessions < ActiveRecord::Migration[5.2]
  def change
    create_table :buyer_sessions do |t|
      t.date :startDate
      t.date :endDate
      t.integer :sessionNum
      t.references :buyer, foreign_key: true

      t.timestamps
    end
  end
end
