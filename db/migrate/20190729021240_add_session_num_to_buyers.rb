class AddSessionNumToBuyers < ActiveRecord::Migration[5.2]
  def change
    add_column :buyers, :session_num, :integer
  end
end
